#!/usr/bin/env bash

source ".$1rc"

cd backend
npm i && sls deploy --stage $1
cd ../ui
npm i && npm run build:test
aws s3 sync dist/ s3://employeohionow-$1-ui
# until i get more immutable resources and cache-control setup we need to invalidate the cloudfront cache after deploying :(
aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_DISTRIBUTION --paths "/*"