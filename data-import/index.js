const fetch = require('node-fetch');
const uuid = require('uuid4');
const imageDataURI = require('image-data-uri');
const fs = require('fs');
const {promisify} = require('util');
const {chunkPromise, PromiseFlavor} = require('chunk-promise');
const _ = require('lodash');

const writeFileAsync = promisify(fs.writeFile);

// file is cached as 'submissions.json' in git
const dataUrl = 'https://webform-designer.iop.ohio.gov/stage-authoring-workforce/postjob/submission?limit=1000';
let DEFAULT_LOGO;

const getDefaultLogo = async () => {
  if(!DEFAULT_LOGO) {
    DEFAULT_LOGO = await imageDataURI.encodeFromFile('./default-logo.svg');
  }
  return DEFAULT_LOGO;
};

const getMatchingArrayItems = (knownValues, inputValues) => {
  return _.chain(knownValues)
    .intersectionWith(inputValues, (a,b) => a.toLowerCase() === b.toLowerCase())
    .uniq()
    .value();
};

const mapSubmissionData = knownLocations => submissionData => {
  //get a list of the matching locations we have in our system
  const locationsFromSubmissionData = _.chain(submissionData.data.openingsLocations || [])
    .map(x => x.openingsLocationsCity.trim())
    .uniq()
    .value();

  const locations = getMatchingArrayItems(knownLocations, locationsFromSubmissionData);

  //map basic data over
  const signup = {
    id: uuid(),
    logo: submissionData.data.logoURL || '',
    name: submissionData.data.employerName,
    openings: submissionData.data.positionsAvailable,
    locations: locations,
    industries: [],//NOTHING TO MAP FROM DATA
    link: submissionData.data.companyPostingURL,
    phone_number: submissionData.data.employerPhoneNumber,
    email: submissionData.data.employerContactEmail,
    //the following is just flags incase we need to resync or something of that nature
    _fromStateSubmissions: true,
    _id: submissionData._id,
  };
  return {signup, submissionData};
};

const tryAndGetLogos = async data => {
  //attempt to get logos
  const {signup} = data;
  //cache logo and reset to nothing
  const logo = signup.logo;
  signup.logo = await getDefaultLogo();
  try {
    if (logo.startsWith('http')) {
      const resp = await fetch(logo, {
        method: 'GET',
      });

      const contentType = resp.headers.get('content-type') || '';
      if (resp.ok && contentType.startsWith('image')) {
        const buffer = await resp.buffer();
        signup.logo = imageDataURI.encode(buffer, contentType);
        console.log(`processed logo into data uri`);
      } else {
        console.log(`image came back with content-type that is not image: "${contentType}"`)
      }
    }
  } catch (err) {
    console.log('error getting image');
    console.error(err);
  }
  return signup;
};

const pushToApi = async (apiBaseUrl, token, signups) => {
  const functionsArePosts = signups.map(s => async () => {
    const resp = await fetch(`${apiBaseUrl}/jobs`, {
      method: 'POST',
      body: JSON.stringify(s),
    });
    if (!resp.ok) {
      console.error(`error creating signup: ${s.name}`);
    }
  });

  await chunkPromise(functionsArePosts, {
    concurrent: 2,
    promiseFlavor: PromiseFlavor.PromiseAll,
    sleepMs: 200,
  })
};

const getLocations = async (apiBaseUrl) => {
  const resp = await fetch(`${apiBaseUrl}/jobs/locations`);
  return await resp.json();
};

//async main
(async () => {
  const token = process.env.AUTH_TOKEN;
  const apiBaseUrl = process.env.API_BASE_URL;
  if (!token || !apiBaseUrl)
    throw new Error(`must provied AUTH_TOKEN and API_BASE_URL to run!`);

  //fetch data
  const response = await fetch(dataUrl);
  if (!response.ok) {
    console.log(`bad request: ${dataUrl}`);
    return;
  }
  const data = await response.json();
  await writeFileAsync('.cached-data/raw-submissions.json', JSON.stringify(data, null, 2));
  console.log(`pulled ${data.length} submissions from state.`);

  //map to our data source
  const knownLocations = await getLocations(apiBaseUrl);
  const functionsAroundPromiseChain = data
    .map(mapSubmissionData(knownLocations))
    .filter(data => data.signup.locations.length > 0)
    //creates a lazy function around promise for chunkPromise
    .map(data => () => tryAndGetLogos(data));

  console.log(`removed ${data.length - functionsAroundPromiseChain.length} submissions due to no matching location, processing ${functionsAroundPromiseChain.length}`);

  const signups = await chunkPromise(functionsAroundPromiseChain, {
    concurrent: 2,
    promiseFlavor: PromiseFlavor.PromiseAll,
    sleepMs: 500,
  });
  await writeFileAsync('.cached-data/signups.json', JSON.stringify(signups, null, 2));

  //push to api
  await pushToApi(apiBaseUrl, token, signups);

  console.log(`processed ${signups.length} job submissions`);

})().catch(err => console.error(err));