const fs = require('fs');
const {promisify} = require('util');
const _ = require('lodash');

const readFileAsync = promisify(fs.readFile);

//async main
(async () => {
  const fileContents = await readFileAsync('./city_state_zip_codes.csv');

  const cities = _.chain(fileContents.toString().split('\r\n'))
    .slice(1)
    .map(line => line.match(/,"([\w\s]*), OH",/))
    .map(match => match[1])
    .uniq()
    .orderBy()
    .value();

  console.log(JSON.stringify(cities))

})().catch(err => console.error(err));