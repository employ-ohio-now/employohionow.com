#!/usr/bin/env bash
set -e

USERPOOL_ID=$(aws cloudformation describe-stack-resources --stack-name employohionow-$1 | jq -r  '.StackResources[] | select(.ResourceType == "AWS::Cognito::UserPool").PhysicalResourceId')

# remove first argument that was stage name
shift;
for EMAIL in "$@"
do
    PASSWORD=$(cat /dev/random | LC_CTYPE=C tr -dc "a-zA-Z0-9-_" | head -c 24)
    aws cognito-idp  admin-create-user --user-pool-id $USERPOOL_ID --username $EMAIL --temporary-password "$PASSWORD" --user-attributes Name=email,Value=$EMAIL --message-action SUPPRESS > /dev/null

  echo "username:$EMAIL password:$PASSWORD"
done

