# overview

using the (serverless framework)[https://serverless.com/] to manage and deploy the api.  it builds up s3/lambda/api gateway/iam roles automatically.

### deployment to dev
```
npm i && sls deploy --stage dev
```

### run local function
```
sls invoke local --stage dev --function getJobIndustries
```
