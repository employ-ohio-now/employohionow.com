const {makeHandler} = require('../makeHandler');
const corsPipeline = require('../pipelines/cors');
const restWithS3 = require('../pipelines/restWithS3');
const requestBodyAsJson = require('../pipelines/requestBodyAsJson');
const _ = require('lodash');
const emailRegex = require('email-regex');
const uuid4 = require('uuid4');
const imageDataUri = require('image-data-uri');
const {URLSearchParams} = require('url');
const fetch = require('node-fetch');
const {getJobsData, createS3File, createLogoFile} = require('../storageService');
const CONSTANTS = require('../constants');

const BUCKET_NAME = process.env.STORAGE_BUCKET_NAME;

const notEmptyString = data => typeof data === "string" && data.length > 0;
const isLink = data => data && /((https?:\/\/)?(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/.test(data);
const isEmail = data => data && emailRegex().test(data);
const isLogo = data => /^(data:image|https?:\/\/)/.test(data);

const validateFormData = async (formData) => {
  const v = [
    ['name', notEmptyString],
    ['phone_number', notEmptyString],
    ['locations', _.isArray], //TODO: check against known locations...
    ['industries', _.isArray], //TODO: check against known industries...
    ['link', isLink],
    ['email', isEmail],
    ['openings', _.isNumber],
    ['logo', isLogo],
  ];

  for (let i = 0; i < v.length; i++) {
    const key = v[i][0];
    const validateFunc = v[i][1];
    if (!validateFunc(formData[key])) {
      console.log(`${key} not valid`);
      return false;
    }
  }

  return true;
};

const recaptchaCheck = async(next, event) => {
  const formData = event.body;
  //check recaptcha
  try {
    const params = new URLSearchParams();
    params.append('secret', process.env.RECAPTCHA_SECRET_KEY);
    params.append('response', formData['recaptcha']);
    const resp = await fetch('https://www.google.com/recaptcha/api/siteverify', {
      method: 'POST',
      body: params,
    });
    if (!resp.ok) {
      console.error('bad resp', resp);
      return false;
    }

    const data = await resp.json();
    if (!data.success){
      console.log('recaptcha response', data);
      return {
        statusCode: 400,
      };
    }
    return await next();

  } catch (err) {
    console.error('recaptcha error', err);
    return {
      statusCode: 400,
    }
  }
};

const validFormDataPipeline = async (next, event) =>{
  const isValid = await validateFormData(event.body);
  if(isValid)
    return await next();

  return {
    statusCode: 400,
  }
};

const createLogoFromDataUri = options => async(next, event) => {
  const {logoKey} = options;
  const logo = event.body.logo;
  if(logo.startsWith('data:')) {
    const decodedImageData = imageDataUri.decode(logo);
    const logoUrl = await createLogoFile(logoKey(event), decodedImageData.dataBuffer, decodedImageData.imageType);
    if (!logoUrl) {
      console.error(`error creating logofile`);
      return {
        statusCode: 400,
      }
    }

    event.body.logo = logoUrl;
  }
  return await next();
};

const mapData = async(next, event) => {
  const id = uuid4();
  event.body = Object.assign(
    {},
    event.body,
    {approved: false, id, recaptcha: undefined});
  return await next();
};

module.exports.postSignups = makeHandler([
  corsPipeline,
  requestBodyAsJson,
  validFormDataPipeline,
  recaptchaCheck,
  mapData,
  createLogoFromDataUri({
    logoKey: e => e.body.id,
  }),
  restWithS3.post({
    bucket: BUCKET_NAME,
    key: event => `${CONSTANTS.S3.SIGNUPS_PREFIX}/${event.body.id.toString()}.json`
  }),
]);

module.exports.getSignups = makeHandler([
  corsPipeline,
  restWithS3.list({bucket: BUCKET_NAME, prefix: CONSTANTS.S3.SIGNUPS_PREFIX}),
]);

module.exports.updateSignups = makeHandler([
  corsPipeline,
  requestBodyAsJson,
  validFormDataPipeline,
  createLogoFromDataUri({
    logoKey: e => `${e.body.id}_${Date.now()}`
  }),
  restWithS3.put({
    bucket: BUCKET_NAME,
    key: event => `${CONSTANTS.S3.SIGNUPS_PREFIX}/${event.body.id.toString()}.json`
  }),
]);

module.exports.deleteSignups = makeHandler([
  corsPipeline,
  restWithS3.remove({
    bucket: BUCKET_NAME,
    key: event => `${CONSTANTS.S3.SIGNUPS_PREFIX}/${event.pathParameters.id}.json`
  })
]);

module.exports.getSignupsById = makeHandler([
  corsPipeline,
  restWithS3.get({
    bucket: BUCKET_NAME,
    key: event => `${CONSTANTS.S3.SIGNUPS_PREFIX}/${event.pathParameters.id}.json`
  })
]);