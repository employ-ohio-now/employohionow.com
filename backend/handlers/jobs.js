const {makeHandler} = require('../makeHandler');
const corsPipeline = require('../pipelines/cors');
const restWithS3 = require('../pipelines/restWithS3');
const CONSTANTS = require('../constants');

const BUCKET_NAME = process.env.STORAGE_BUCKET_NAME;

module.exports.getJobs = makeHandler([
  corsPipeline,
  restWithS3.get({key: CONSTANTS.S3.JOBS_FILE, bucket: BUCKET_NAME}),
]);

module.exports.getJobLocations = makeHandler([
  corsPipeline,
  restWithS3.get({key: CONSTANTS.S3.LOCATIONS_FILE, bucket: BUCKET_NAME}),
]);

module.exports.getJobIndustries = makeHandler([
  corsPipeline,
  restWithS3.get({key: CONSTANTS.S3.INDUSTRIES_FILE, bucket: BUCKET_NAME}),
]);