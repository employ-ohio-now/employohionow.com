const {
  getJobSignups,
  createJobsData,
} = require('../storageService');

module.exports.adminCreateMasterJobList = async () => {
  try {
    const signups = await getJobSignups();
    if(!signups) {
      console.error('error retrieving jobs');
      return false;
    }

    const currentLiveJobs = signups.filter(s => s.approved);
    return await createJobsData(currentLiveJobs);
  } catch (err) {
    console.error(err);
    return false;
  }
};