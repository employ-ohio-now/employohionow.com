const aws = require('aws-sdk');
const s3 = new aws.S3();
const _ = require('lodash');

const get = options => async (nextHandler, event) => {
  if (nextHandler)
    console.warn(`get handler expects to be last pipeline, 'nextHandler' was not undefined. pipelines after this will not be called.`);

  const {bucket, key} = options;
  const keyValue = _.isFunction(key)
    ? key(event)
    : key;

  try {
    const resp = await s3.getObject({
      Bucket: bucket,
      Key: keyValue,
    }).promise();
    return {
      statusCode: 200,
      body: resp.Body.toString(),
    }
  } catch (err) {
    //handle 404
    if (err.code === 'NoSuchKey') {
      return {
        statusCode: 404,
      }
    }

    // unhandled errors propagate
    console.error(err);
    throw err;
  }
};

const post = options => async (next, event) => {
  if (next)
    console.warn(`get handler expects to be last pipeline, 'nextHandler' was not undefined. pipelines after this will not be called.`);
  //todo: check for existence and throw if already there?

  const {bucket, key, acl, contentType} = options;
  const keyValue = _.isFunction(key)
    ? key(event)
    : key;

  await s3.putObject({
    Bucket: bucket,
    Key: keyValue,
    Body: Buffer.from(JSON.stringify(event.body)),
    ACL: acl || 'private',
    ContentType: contentType || 'application/json',
  }).promise();

  return {
    statusCode: 204, //note: 200 so we can have body? 204 signals to drop anything other than header
    body: JSON.stringify(event.body),
  }
};

const put = options => async (next, event) => {
  if (next)
    console.warn(`get handler expects to be last pipeline, 'nextHandler' was not undefined. pipelines after this will not be called.`);

  //todo: check for existence and throw if nothing existing?

  const {bucket, key, acl, contentType} = options;
  const keyValue = _.isFunction(key)
    ? key(event)
    : key;

  await s3.putObject({
    Bucket: bucket,
    Key: keyValue,
    Body: Buffer.from(JSON.stringify(event.body)),
    ACL: acl || 'private',
    ContentType: contentType || 'application/json',
  }).promise();

  return {
    statusCode: 200,
    body: JSON.stringify(event.body),
  }
};


const list = options => async (next, event) => {
  if (next)
    console.warn(`get handler expects to be last pipeline, 'nextHandler' was not undefined. pipelines after this will not be called.`);

  const objects = await s3.listObjectsV2({
    Bucket: options.bucket,
    Prefix: options.prefix,
  }).promise();

  //TODO: handle more than 1000 signups, IE continuationToken
  const fetches = objects.Contents
    .map(async o => {
      const data = await s3.getObject({Bucket: options.bucket,Key:o.Key}).promise();
      return JSON.parse(data.Body);
    });

  return {
    statusCode: 200,
    body: JSON.stringify(await Promise.all(fetches)),
  }
};

const remove = options => async(next, event) => {
  if (next)
    console.warn(`get handler expects to be last pipeline, 'nextHandler' was not undefined. pipelines after this will not be called.`);

  const {key} = options;
  const keyValue = _.isFunction(key)
    ? key(event)
    : key;

  await s3.deleteObject({
    Bucket: options.bucket,
    Key: keyValue,
  }).promise();

  return {
    statusCode: 204,
  };
};

module.exports = {get, post, list, put, remove};