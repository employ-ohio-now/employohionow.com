/**
 * replaces request event 'body' with a parsed json object
 * @param nextHandler
 * @param event
 * @returns {Promise<*>}
 */
module.exports = async (nextHandler, event) => {
  event.body = JSON.parse(event.body);
  return await nextHandler();
}