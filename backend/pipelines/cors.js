const corsHeaders = {
  'Access-Control-Allow-Origin': '*',
};

module.exports = async (nextHandler, event) => {
  const resp = await nextHandler();
  return Object.assign(
    {},
    resp,
    {headers: Object.assign({}, resp.headers || {}, corsHeaders)})
};