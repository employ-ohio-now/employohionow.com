module.exports = {
  S3: {
    SIGNUPS_PREFIX: 'job_signups',
    LOGOS_PREFIX: 'logos',
    JOBS_FILE: 'jobs.json',
    LOCATIONS_FILE: 'locations.json',
    INDUSTRIES_FILE: 'industries.json',
  },
};