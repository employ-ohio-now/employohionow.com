const assert = require('assert');
const {makeHandler} = require('./index');

const simpleAsyncPipeline = async (nextHandler, event) => {
  console.log(nextHandler, event);
  return {simpleAsyncPipeline: true};
};

const createSimpleContinueing = (name, isLastHandler=false) => async (nextHandler, event) => {
  console.log('createSimpleContinueing', name, nextHandler);
  let nextHandlerValue = {}; //default to nothing
  if(isLastHandler) {
    assert(typeof nextHandler === "undefined", "last handler should be undefined");
  } else {
    nextHandlerValue = await nextHandler();
  }

  event[name] = name;

  return Object.assign({},
    nextHandlerValue,
    {[name]: name});
}

describe('makeHandler', () => {

  it('should work with one async pipeline', async function () {
    const handler = makeHandler([simpleAsyncPipeline]);
    const resp = await handler({});
    assert.deepStrictEqual(resp, {simpleAsyncPipeline: true});
  });

  it('should work with two or more pipelines', async function () {
    const handler = makeHandler([
      createSimpleContinueing('1'),
      createSimpleContinueing('2'),
      createSimpleContinueing('3', true),
    ]);
    const event = {};
    const resp = await handler(event);
    assert.deepStrictEqual(resp, {'1': '1', '2': '2', '3': '3'});
    assert.deepStrictEqual(event, {'1': '1', '2': '2', '3': '3'});
  });
});