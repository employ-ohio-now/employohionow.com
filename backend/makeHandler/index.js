/**
 * takes a list of pipelines and runs them in order to give pipeline to modify things on the way in and on the way out.

 * @param pipelines
 * @returns {function(*=): *} hands back an async function that takes in one 'event' argument
 */
module.exports.makeHandler = pipelines => {
  return async (lambdaEvent) => {

    const createNextHandler = (currentPipelineIndex) => {
      const pipelineToRunIndex = currentPipelineIndex + 1;
      // the last pipeline's 'nextHandler' is undefined so it knows its the end
      if (pipelineToRunIndex >= pipelines.length) {
        return undefined;
      }

      return async () => {
        return await pipelines[pipelineToRunIndex](createNextHandler(pipelineToRunIndex), lambdaEvent);
      };
    };
    return await pipelines[0](createNextHandler(0), lambdaEvent);
  }
};