/*
just a simple wrapper around storage stuff now, which is s3 currently
 */
const aws = require('aws-sdk');
const CONSTANTS = require('./constants');
const s3 = new aws.S3();

const BUCKET_NAME = process.env.STORAGE_BUCKET_NAME;
const MASTER_JOBS_LIST_S3_KEY = 'jobs.json';

const createJobsData = async (jobs) => {
    return await createS3File(MASTER_JOBS_LIST_S3_KEY,
      Buffer.from(JSON.stringify(jobs))
    );
};

const getJobsData = async () => {
    try {
        const resp = await s3.getObject({
            Bucket: BUCKET_NAME,
            Key: MASTER_JOBS_LIST_S3_KEY,
        }).promise();

        return JSON.parse(resp.Body);
    } catch (err) {
        console.error(err);
        return [];
    }
};

const createS3File = async (key,
                            fileBuffer,
                            acl='private',
                            contentType=undefined) => {
    try {
        await s3.putObject({
            Bucket: BUCKET_NAME,
            Key: key,
            Body: fileBuffer,
            ACL: acl,
            ContentType: contentType,
        }).promise();

        return true;
    } catch (err) {
        console.error(err);
        return false;
    }
};

const createLogoFile = async(fileName, fileBuffer, contentType) => {
    const key = `${CONSTANTS.S3.LOGOS_PREFIX}/${fileName}`;
    const success = await createS3File(key, fileBuffer, 'public-read', contentType);
    if(!success)
        return false;

    return `https://${BUCKET_NAME}.s3.amazonaws.com/${key}`;
};

const getJobSignups = async() => {
    try {
        const objects = await s3.listObjectsV2({
            Bucket: BUCKET_NAME,
            Prefix: CONSTANTS.S3.SIGNUPS_PREFIX,
        }).promise();
        //TODO: handle more than 1000 signups, IE continuationToken
        const fetches = objects.Contents
          .map(async o => {
            const data = await s3.getObject({Bucket: BUCKET_NAME,Key:o.Key}).promise();
            return JSON.parse(data.Body);
        });

        return Promise.all(fetches);
    } catch (err) {
        console.error(err);
        return false;
    }
};

const updateJobSignup = async(id, signup) => {
    const key = `${CONSTANTS.S3.SIGNUPS_PREFIX}/${id}.json`;
    const buffer = Buffer.from(JSON.stringify(signup));
    return await createS3File(key, buffer);
};

module.exports = {getJobsData, createS3File, createLogoFile, getJobSignups, updateJobSignup, createJobsData};