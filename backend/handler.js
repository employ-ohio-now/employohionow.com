'use strict';

const {
    getJobs,
    getJobLocations,
    getJobIndustries,
} = require('./handlers/jobs');
const {
    postSignups,
    getSignups,
    updateSignups,
    deleteSignups,
    getSignupsById,
} = require('./handlers/signups');
const {
    adminCreateMasterJobList,
} = require('./handlers/admin');


//jobs
module.exports.getJobs = getJobs;
module.exports.getJobLocations = getJobLocations;
module.exports.getJobIndustries = getJobIndustries;

//signups
module.exports.postSignups = postSignups;
module.exports.getSignups = getSignups;
module.exports.updateSignups = updateSignups;
module.exports.deleteSignups = deleteSignups;
module.exports.getSignupsById = getSignupsById;


module.exports.adminCreateMasterJobList = async event => {
    await adminCreateMasterJobList();
    return {
        statusCode: 200,
    }
};