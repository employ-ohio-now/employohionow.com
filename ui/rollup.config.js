import svelte from "rollup-plugin-svelte";
import resolve from "@rollup/plugin-node-resolve";
import buble from "@rollup/plugin-buble";
import commonjs from "@rollup/plugin-commonjs";
import postcss from "rollup-plugin-postcss";
import serve from "rollup-plugin-serve";
import livereload from "rollup-plugin-livereload";
import injectProcessEnv from 'rollup-plugin-inject-process-env';
import { terser } from "rollup-plugin-terser";
import polyfill from "rollup-plugin-polyfill";

// load environment variables.  easy to put this in a .env file in the root ui directory
const { API_BASE_URL, ADMIN_LOGIN_URL } = process.env;
if(!API_BASE_URL || !ADMIN_LOGIN_URL)
  throw new Error(`API_BASE_URL and ADMIN_LOGIN_URL need to be provided to run`);

const production = !process.env.ROLLUP_WATCH;

console.log("is prod:", production);
export default {
  input: "src/main.js",
  output: {
    sourcemap: true,
    format: "iife",
    name: "app",
    compact: true,
    file: production ? "dist/bundle.js" : "public/bundle.js"
  },
  plugins: [

    svelte({
      // enable run-time checks when not in production
      dev: !production,
      emitCss: true,
      // we'll extract any component CSS out into
      // a separate file — better for performance
      css: css => {
        css.write(production ? "dist/bundle.css" : "public/bundle.css");
      }
    }),

    postcss({
      extract: production ? "dist/bundle.css" : "public/bundle.css"
    }),

    // If you have external dependencies installed from
    // npm, you'll most likely need these plugins. In
    // some cases you'll need additional configuration —
    // consult the documentation for details:
    // https://github.com/rollup/rollup-plugin-commonjs
    resolve({
      browser: true,
      dedupe: importee =>
        importee === "svelte" || importee.startsWith("svelte/")
    }),
    commonjs(),
    buble({
      transforms: { forOf: false, asyncAwait: false }
    }),

    polyfill(['array-flat-polyfill']),

    injectProcessEnv({
      API_BASE_URL,
      ADMIN_LOGIN_URL,
      RECAPTCHA_SITE_KEY: process.env.RECAPTCHA_SITE_KEY,
      GTM_ID: process.env.GTM_ID,
    }),

    // If we're building for production (npm run build
    // instead of npm run dev), minify
    production && terser(),
    !production &&
      serve({
        contentBase: "public",
        port: 9000
      }),
    !production && livereload()
  ],
  watch: {
    clearScreen: true
  }
};
