export default [
  {
    logo: "scriptdrop-logo.jpg",
    name: "Scriptdrop",
    openings: 6,
    locations: ["Columbus"],
    industries: ["Operations", "Finance", "Marketing", "Technology"],
    link: "https://scriptdrop.bamboohr.com/jobs/?source=other"
  },
  {
    logo: "midohiofoodbank-logo.jpg",
    name: "Mid-Ohio Food Bank",
    openings: 3,
    locations: ["Grove City"],
    industries: [
      "Truck Driver",
      "Technology",
      "Operations",
      "Customer Service"
    ],
    link: "https://midohiofoodbank.applicantpro.com/jobs/"
  },
  {
    logo: "amazon-logo.jpg",
    name: "Amazon",
    openings: 600,
    locations: ["Dublin", "Hilliard", "West Jefferson"],
    industries: ["Operations", "Technology", "Warehouse", "Customer Service"],
    link:
      "https://www.amazon.jobs/en/search?base_query=&loc_query=Columbus+Metropolitan+Area%2C+OH%2C+United+States&loc_group_id=columbus-metropolitan-area"
  },
  {
    logo: "dollartree-logo.jpg",
    name: "Dollar Tree",
    openings: 15,
    locations: ["Columbus"],
    industries: ["Cashiers", "Stockers"],
    link: "https://www.familydollar.com/careers"
  },
  {
    logo: "meijer-logo.jpg",
    name: "Meijer",
    openings: 270,
    locations: ["Columbus"],
    industries: ["Cashiers", "Stockers"],
    link: "https://meijercareers.dejobs.org/ohio/usa/jobs/"
  },
  {
    logo: "dominos-logo.jpg",
    name: "Dominos Pizza",
    openings: 80,
    locations: ["Columbus"],
    industries: ["Delivery", "Customer Service", "Operations"],
    link: "https://www.indeed.com/q-Dominos-Pizza-l-Ohio-jobs.html"
  },
  {
    logo: "unitedhealthgroup-logo.jpg",
    name: "United Health Group",
    openings: 20,
    locations: ["Columbus", "Westerville", "New Albany"],
    industries: ["Delivery", "Customer Service", "Operations"],
    link:
      "https://careers.unitedhealthgroup.com/job-search#sort=%40openingz32xdatez32xez120xt%20descending&country=United%20States"
  },
  {
    logo: "Fortuity-logo.jpg",
    name: "Fortuity",
    openings: 5,
    locations: ["Columbus"],
    industries: ["Customer Service"],
    link:
      "https://careers.unitedhealthgroup.com/job-search#sort=%40openingz32xdatez32xez120xt%20descending&country=United%20States"
  },
  {
    logo: "ContactUS-logo.jpg",
    name: "ContactUS",
    openings: 2,
    locations: ["Columbus"],
    industries: ["Customer Service"],
    link: "http://www.contactusinc.com/careers/"
  },
  {
    logo: "scriptdrop-logo.jpg",
    name: "Scriptdrop",
    openings: 6,
    locations: ["Columbus"],
    industries: ["Operations", "Finance", "Marketing", "Technology"],
    link: "https://scriptdrop.bamboohr.com/jobs/?source=other"
  },
  {
    logo: "midohiofoodbank-logo.jpg",
    name: "Mid-Ohio Food Bank",
    openings: 3,
    locations: ["Grove City"],
    industries: [
      "Truck Driver",
      "Technology",
      "Operations",
      "Customer Service"
    ],
    link: "https://midohiofoodbank.applicantpro.com/jobs/"
  },
  {
    logo: "amazon-logo.jpg",
    name: "Amazon",
    openings: 600,
    locations: ["Dublin", "Hilliard", "West Jefferson"],
    industries: ["Operations", "Technology", "Warehouse", "Customer Service"],
    link:
      "https://www.amazon.jobs/en/search?base_query=&loc_query=Columbus+Metropolitan+Area%2C+OH%2C+United+States&loc_group_id=columbus-metropolitan-area"
  },
  {
    logo: "dollartree-logo.jpg",
    name: "Dollar Tree",
    openings: 15,
    locations: ["Columbus"],
    industries: ["Cashiers", "Stockers"],
    link: "https://www.familydollar.com/careers"
  },
  {
    logo: "meijer-logo.jpg",
    name: "Meijer",
    openings: 270,
    locations: ["Columbus"],
    industries: ["Cashiers", "Stockers"],
    link: "https://meijercareers.dejobs.org/ohio/usa/jobs/"
  },
  {
    logo: "dominos-logo.jpg",
    name: "Dominos Pizza",
    openings: 80,
    locations: ["Columbus"],
    industries: ["Delivery", "Customer Service", "Operations"],
    link: "https://www.indeed.com/q-Dominos-Pizza-l-Ohio-jobs.html"
  },
  {
    logo: "unitedhealthgroup-logo.jpg",
    name: "United Health Group",
    openings: 20,
    locations: ["Columbus", "Westerville", "New Albany"],
    industries: ["Delivery", "Customer Service", "Operations"],
    link:
      "https://careers.unitedhealthgroup.com/job-search#sort=%40openingz32xdatez32xez120xt%20descending&country=United%20States"
  },
  {
    logo: "Fortuity-logo.jpg",
    name: "Fortuity",
    openings: 5,
    locations: ["Columbus"],
    industries: ["Customer Service"],
    link:
      "https://careers.unitedhealthgroup.com/job-search#sort=%40openingz32xdatez32xez120xt%20descending&country=United%20States"
  },
  {
    logo: "ContactUS-logo.jpg",
    name: "ContactUS",
    openings: 2,
    locations: ["Columbus"],
    industries: ["Customer Service"],
    link: "http://www.contactusinc.com/careers/"
  }
];
