const ADMIN_URL = process.env.ADMIN_LOGIN_URL;
const apiUrl = process.env.API_BASE_URL;

const API = {
  getJobs: `${apiUrl}/jobs`,
  getLocations: `${apiUrl}/jobs/locations`,
  getIndustries: `${apiUrl}/jobs/industries`,
  postJob: `${apiUrl}/signups`,
  getSignups: `${apiUrl}/signups`,
  getSignupsById: id => `${apiUrl}/signups/${id}`,
  putSignups: (id) => `${apiUrl}/signups/${id}`,
}

export { API, ADMIN_URL }
