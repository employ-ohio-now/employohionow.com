import "whatwg-fetch";
import App from "./App.svelte";
import { jwtStore, locationsStore, industriesStore } from "./stores.js";
import { API } from "./consts.js";
import { push } from "svelte-spa-router";
import qs from "qs";

let tokenRegex = /#id_token=.*/;

if (tokenRegex.test(window.location.hash)) {
  const jwtQueryStr = window.location.hash.substring(10);
  jwtStore.set(qs.parse(jwtQueryStr).access_token);
  push("/admin");
}

// dev hack, let 404 happen via web server
if (!window.location.hash || window.location.hash == "#") {
  window.location = "/#" + window.location.pathname;
}

// start off a few requests to seed/update data
fetch(
  API.getLocations
)
  .then(response => response.json())
  .then(data => {
    locationsStore.set(data);
  })
  .catch(err => {
    console.error(err);
  });

fetch(
  API.getIndustries
)
  .then(response => response.json())
  .then(data => {
    industriesStore.set(data);
  })
  .catch(err => {
    console.log(err);
  });

//setup gtm
if(process.env.GTM_ID) {
  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer',process.env.GTM_ID);
}

const app = new App({
  target: document.querySelector("#svelte"),
  props: {}
});

export default app;
