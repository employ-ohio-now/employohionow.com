const postcssImport = require( "postcss-import");
const cssnano = require( "cssnano");
const autoprefixer = require( "autoprefixer");
const tailwind = require( "tailwindcss");
const purgecss = require( "@fullhuman/postcss-purgecss");

const production = !process.env.ROLLUP_WATCH;

const removeUnusedCss = purgecss({
  // Specify the paths to all of the template files in your project
  content: ["./public/**/*.html", "./src/**/*.html", "./src/**/*.svelte"],
  whitelistPatterns: [/svelte-/],
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
});

module.exports = {
  plugins: [
    postcssImport,
    autoprefixer,
    tailwind(),
    production && removeUnusedCss,
    cssnano()
  ].filter(Boolean),
}