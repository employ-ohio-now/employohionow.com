# overview for deploying

to deploy you need the following
- aws cli setup with creds
- sls setup with creds

you can run the following command to deploy an environment.
```bash
bash deploy.sh <env>
```

you will need to create a file called `.<env>rc` that just loads the appropriate environment variables to deploy.  this is a list of what you currently need to provide.

```
export RECAPTCHA_SITE_KEY="..."
export RECAPTCHA_SECRET_KEY="..."
export API_BASE_URL="..."
export CLOUDFRONT_DISTRIBUTION="..."
```

### first time deployment

variables currently needed require a deployment of the `backend` project to create infrastructure, and then you can fill them in.  it can be changed to pull from cloudformation stack in future.

### SSL with custom domain (employcolumbusnow.org)
just manually created a SSL certificate using Amazon Certificate Manager.  We did DNS verification.  After that is verified I attached the certificate to cloudfront and setup the hostname and pointed our DNS CNAME to cloudfront.


### creating new users
```bash
bash setupNewUser.sh <stage> <email1> <email2>
```

example would be for dev
```bash
bash setupNewUser.sh dev a@a.com b@b.com c@c.com
```

### resetting password
```bash
bash resetUserPassword.sh <stage> <email>
```