#!/usr/bin/env bash
set -e

EMAIL=$2
USERPOOL_ID=$(aws cloudformation describe-stack-resources --stack-name employohionow-$1 | jq -r '.StackResources[] | select(.ResourceType == "AWS::Cognito::UserPool").PhysicalResourceId')

PASSWORD=$(cat /dev/random | LC_CTYPE=C tr -dc "a-zA-Z0-9-_" | head -c 24)
aws cognito-idp admin-set-user-password --user-pool-id $USERPOOL_ID --username $EMAIL --password "$PASSWORD" --no-permanent

echo "username:$EMAIL password:$PASSWORD"
